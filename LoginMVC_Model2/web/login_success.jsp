<%-- 
    Document   : login_success
    Created on : Jan 4, 2019, 10:45:29 AM
    Author     : phamq
--%>
<%@page import="bean.LoginBean" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <style>
            .title{margin: 10px auto; font-size: 20px; width: 320px; font-weight: bold}
            .titlep{margin-top: 50px}
        </style>
    </head>
    <body>
        <!--${requestScope.bean.name}-->
        <form action="ControllerServlet" method="post">
            <p class="title titlep" >You are successfully logged in</p>
            <p class="title">
                <jsp:useBean id="bean" class="bean.LoginBean" scope="request" />
            <h3>Welcome, <a style='color: rgb(37, 232, 151); margin: center'>
                    <%=bean.getName()%></a>
                    <%--<%@include file="index.html" %>--%>
            </p>
        </form>
    </body>
</html>
